﻿(function ($) {

    $.fn.showLoader = function (options) {
        kendo.ui.progress($(this), true);
    };
    $.fn.hideLoader = function (options) {
        kendo.ui.progress($(this), false);
    };

    $(function () {

        const root = $("#app");
        const amountElement = root.find('input[name=amount]');

        let transaction = {
            data: {
                senderId: '',
                recipientId: '',
                amount: '',
                confirmed: false
            },
            validate: function () {
                let valid = true;
                let senderAccount = senderAccountDropdown.dataSource.get(transaction.data.senderId);
                if (this.data.amount > senderAccount.amount) {
                    valid = valid && false;
                    amountElement.addClass('is-invalid').next('.invalid-feedback').text('Сумма транзакции больше суммы на счете');
                }
                else {
                    amountElement.removeClass('is-invalid');
                }
                return valid;
            },
            submit: function () {
                if (this.validate()) {
                    let senderBank = senderBankDropdown.dataSource.get(senderBankDropdown.value());
                    let that = this;
                    if (senderBank.requireConfirmation) {
                        kendo.confirm("Вы подтерждаете операцию?").then(function () {
                            that.data.confirmed = true;
                            that.send();
                        }, function () {
                            
                        });
                    }
                    else
                        this.send();
                }
            },
            send: function () {
                $('input').removeClass('is-invalid');
                $('.alert-danger').addClass('d-none');
                root.find('.alert-success').addClass('d-none');
                transaction.data.amount = transaction.data.amount ? transaction.data.amount : 0;
                popup.wrapper.showLoader();
                $.post('api/transaction', transaction.data, function (response) {
                    popup.wrapper.hideLoader();
                    popup.close();
                    root.find('.alert-success').removeClass('d-none').text(response.message);
                    setTimeout(function () {
                        root.find('.alert-success').addClass('d-none');
                    }, 3000);
                    senderAccountDropdown.dataSource.read({
                        bankId: senderBankDropdown.value()
                    });
                })
                    .fail(function (response) {
                        popup.wrapper.hideLoader();
                        response = response.responseJSON;
                        let errors = response.errors || response;
                        if (errors) {
                            for (let field in errors) {
                                if (field == 'error')
                                    $('.alert-danger').removeClass('d-none').text(errors[field].join(', '));
                                else
                                    $('input[name=' + field + ']').addClass('is-invalid').next('.invalid-feedback').text(errors[field].join(', '))
                            }
                        }
                        else
                            $('.alert-danger').removeClass('d-none').text("Неизвестная ошибка");
                })
            }
        };

        let bankOptions = {
            optionLabel: "Выберите банк...",
            dataTextField: "name",
            dataValueField: "id",
            dataSource: {
                transport: {
                    read: {
                        url: "/api/bank"
                    }
                }
            }
        };

        let accountOptions = {
            optionLabel: "Выберите счет...",
            dataTextField: "name",
            dataValueField: "id",
            template: function (item) {
                return item.accountType.name + ' (' + kendo.toString(item.amount, "##,#.##").replace(/,/g, " ") + ' р.)';
            },
            valueTemplate: function (item) {
                if (item.id !== "")
                    return item.accountType.name + ' (' + kendo.toString(item.amount, "##,#.##").replace(/,/g, " ") + ' р.)';
            },
            dataSource: {
                transport: {
                    read: {
                        url: "/api/bankAccount"
                    }
                }
            }
        };

        const senderSubmit = root.find('.sender_submit').click(function (e) {
            e.preventDefault();
            if (transaction.data.recipientId === '') {
                popup.wrapper.find('form').removeClass('d-none');
                popup.center().open();
            }
            else
                transaction.submit();
        });

        const recipientSubmit = root.find('.recipient_submit').click(function (e) {
            e.preventDefault();
            transaction.submit();
        });

        const senderBankDropdown = root.find(".senderBankDropdown").kendoDropDownList($.extend(true, bankOptions, {
            change: function (e) {
                var value = this.value();
                if (value !== '') {
                    senderAccountDropdown.element.closest('.form-group').removeClass('d-none');
                    senderAccountDropdown.dataSource.read({
                        bankId: value
                    })
                }
                else {
                    senderAccountDropdown.element.closest('.form-group').addClass('d-none');
                }
                senderSubmit.closest('.form-group').addClass('d-none');
            }
        })).data("kendoDropDownList");

        const senderAccountDropdown = root.find(".senderAccountDropdown").kendoDropDownList($.extend(true, accountOptions, {
            change: function (e) {
                var value = this.value();
                transaction.data.senderId = value;
                senderSubmit.closest('.form-group').toggleClass('d-none', value === '');
            }
        })).data("kendoDropDownList");

        const recipientBankDropdown = root.find(".recipientBankDropdown").kendoDropDownList($.extend(true, bankOptions, {
            change: function (e) {
                var value = this.value();
                if (value !== '') {
                    recipientAccountDropdown.element.closest('.form-group').removeClass('d-none');
                    recipientAccountDropdown.dataSource.read({
                        bankId: value
                    })
                }
                else {
                    recipientAccountDropdown.value("");
                    recipientAccountDropdown.trigger('change');
                    recipientAccountDropdown.element.closest('.form-group').addClass('d-none');
                }
                recipientSubmit.closest('.form-group').addClass('d-none');
            }
        })).data("kendoDropDownList");

        const recipientAccountDropdown = root.find(".recipientAccountDropdown").kendoDropDownList($.extend(true, accountOptions, {
            change: function (e) {
                var value = this.value();
                transaction.data.recipientId = value;
                recipientSubmit.closest('.form-group').toggleClass('d-none', value === '');
            },
            template: function (item) {
                return item.accountType.name;
            },
            valueTemplate: function (item) {
                if (item.id !== "")
                    return item.accountType.name;
            }
        })).data("kendoDropDownList");

        root.find('input[type=number]').keyup(function (e) {
            transaction.data.amount = $(this).val();
            transaction.validate();
        });

        const popup = root.find('form:nth-child(2)').kendoWindow({
            title: 'Выберите получателя',
            modal: true,
            visible: false,
            width: '80%',
            draggable: false,
            close: function () {
                $('input').removeClass('is-invalid');
                recipientBankDropdown.value("");
                recipientBankDropdown.trigger('change');
                amountElement.val('');
            }
        }).data('kendoWindow');

    })
})(jQuery)
