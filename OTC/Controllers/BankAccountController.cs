﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OTC.Models;
using OTC.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OTC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankAccountController : ApiController
    {
        public BankAccountController(IRepository repository) : base(repository)
        {

        }

        [HttpGet]
        public IEnumerable<Account> Get(BankId bankId)
        {
            return repository.GetAccounts(bankId);
        }
    }
}
