﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OTC.Data;
using OTC.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OTC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ApiController
    {
        public BankController(IRepository repository): base(repository)
        {

        }

        [HttpGet]
        public IEnumerable<Bank> Get()
        {
            return repository.GetBanks().OrderBy(x => x.Id);
        }
    }
}
