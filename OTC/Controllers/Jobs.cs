﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OTC.Models;

namespace OTC.Controllers
{
    public class Jobs : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        public Jobs(UserManager<ApplicationUser> userManager) : base()
        {
            _userManager = userManager;
        }

        public async Task<IActionResult> AddUser()
        {
            var result = await _userManager.CreateAsync(new ApplicationUser { 
                Email = "operator@mail.ru",
                UserName = "operator@mail.ru"
            }, "Operator_1234");

            return Content("ок");
        }
    }
}
