﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OTC.Models;
using OTC.Data;
using OTC.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OTC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionController : ApiController
    {
        private ITransactionService _transactionService;

        public TransactionController(IRepository repository, ITransactionService transactionService) : base(repository)
        {
            _transactionService = transactionService;
        }

        [HttpGet("{accountId}")]
        public List<Transaction> Get(int accountId)
        {
            var res = repository.GetOutTransactions(accountId);
            res.AddRange(repository.GetInTransactions(accountId));
            return res;
        }

        [HttpPost]
        public object Post([FromForm] Transaction value)
        {
            try
            {
                value.UserId = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                _transactionService.AddTransaction(value);
                return new
                {
                    success = true,
                    message = "Транзакция успешно добавлена"
                };
            }
            catch (TransactionException exception)
            {
                ModelState.AddModelError(exception.field, exception.Message);
                return this.BadRequest(ModelState);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("error", "Неизвестная ошибка");
                return this.BadRequest(ModelState);
            }
        }
    }
}
