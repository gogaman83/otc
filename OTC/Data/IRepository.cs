﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;

namespace OTC.Data
{
    public interface IRepository
    {
        IEnumerable<Bank> GetBanks();
        List<Account> GetAccounts(BankId bankId);
        Account GetAccount(int accountId);
        List<Transaction> GetOutTransactions(int accountId);
        List<Transaction> GetInTransactions(int accountId);
        void AddTransaction(Transaction transaction);
        void Save();
    }
}
