﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OTC.Data.Migrations
{
    public partial class seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "AccountTypeId", "Amount", "BankId", "UID" },
                values: new object[,]
                {
                    { 1, 0, 11000.0, 0, "1c9ec546-3de5-47f9-9618-5e0f9bd5566c" },
                    { 2, 1, 12000.0, 0, "0b6a89e5-bfd0-4b23-8203-576e0470adcc" },
                    { 3, 2, 13000.0, 0, "942bbf9e-fc4c-43de-856b-4e12b7f0eadf" },
                    { 4, 0, 21000.0, 2, "4df9075d-d69e-4c5f-89eb-69a6be873d1c" },
                    { 5, 1, 22000.0, 2, "f11abe97-e328-48a0-b6b8-b87b47bf0fcd" },
                    { 6, 2, 23000.0, 2, "15a24031-fdf2-4be1-a69c-2f1ef024fe63" },
                    { 7, 0, 31000.0, 1, "71d8b52a-71a2-4c00-834f-c93e5a74d017" },
                    { 8, 1, 32000.0, 1, "12fce43e-3b7b-4291-bc4c-73d205b33cc2" },
                    { 9, 2, 33000.0, 1, "550ca059-2125-46e2-98d6-2282c5e88836" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 9);
        }
    }
}
