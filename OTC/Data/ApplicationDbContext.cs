﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OTC.Models;

namespace OTC.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Account>()
                .HasIndex(u => u.UID)
                .IsUnique();

            builder
                .Entity<Account>()
                .HasMany(x => x.OutTransactions)
                .WithOne(x => x.Sender)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .Entity<Account>()
                .HasMany(x => x.InTransactions)
                .WithOne(x => x.Recipient)
                .OnDelete(DeleteBehavior.NoAction);

            //Добавляем счета
            builder.Entity<Account>().HasData(
                new Account() { Id = 1, BankId = BankId.sbrf, AccountTypeId = AccountTypeId.physical, UID = Guid.NewGuid().ToString(), Amount = 11000 },
                new Account() { Id = 2, BankId = BankId.sbrf, AccountTypeId = AccountTypeId.legal, UID = Guid.NewGuid().ToString(), Amount = 12000 },
                new Account() { Id = 3, BankId = BankId.sbrf, AccountTypeId = AccountTypeId.nonresident, UID = Guid.NewGuid().ToString(), Amount = 13000 },
                new Account() { Id = 4, BankId = BankId.alpha, AccountTypeId = AccountTypeId.physical, UID = Guid.NewGuid().ToString(), Amount = 21000 },
                new Account() { Id = 5, BankId = BankId.alpha, AccountTypeId = AccountTypeId.legal, UID = Guid.NewGuid().ToString(), Amount = 22000 },
                new Account() { Id = 6, BankId = BankId.alpha, AccountTypeId = AccountTypeId.nonresident, UID = Guid.NewGuid().ToString(), Amount = 23000 },
                new Account() { Id = 7, BankId = BankId.vtb, AccountTypeId = AccountTypeId.physical, UID = Guid.NewGuid().ToString(), Amount = 31000 },
                new Account() { Id = 8, BankId = BankId.vtb, AccountTypeId = AccountTypeId.legal, UID = Guid.NewGuid().ToString(), Amount = 32000 },
                new Account() { Id = 9, BankId = BankId.vtb, AccountTypeId = AccountTypeId.nonresident, UID = Guid.NewGuid().ToString(), Amount = 33000 }
                );
        }
    }
}
