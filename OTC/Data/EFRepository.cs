﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OTC.Models;

namespace OTC.Data
{
    public class EFRepository : IRepository
    {
        private ApplicationDbContext _context;

        public EFRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Bank> GetBanks()
        {
            return Helpers.GetEnumerableOfType<Bank>();
        }

        public List<Account> GetAccounts(BankId bankId)
        {
            return _context.Accounts.Where(x => x.BankId == bankId).ToList();
        }

        public Account GetAccount(int accountId)
        {
            return _context.Accounts.FirstOrDefault(x => x.Id == accountId);
        }

        public List<Transaction> GetOutTransactions(int accountId)
        {
            return _context.Transactions.Where(x => x.SenderId == accountId).ToList();
        }

        public List<Transaction> GetInTransactions(int accountId)
        {
            return _context.Transactions.Where(x => x.RecipientId == accountId).ToList();
        }

        public void AddTransaction(Transaction transaction)
        {
            _context.Transactions.Add(transaction);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
