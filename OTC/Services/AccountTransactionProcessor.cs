﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;
using Microsoft.AspNetCore.Http;

namespace OTC.Services
{
    public class AccountTransactionProcessor : IAccountTransactionProcessor
    {
        private IEmailSender _emailSender;
        private ISmsSender _smsSender;

        public AccountTransactionProcessor(IEmailSender emailSender, ISmsSender smsSender)
        {
            _emailSender = emailSender;
            _smsSender = smsSender;
        }

        public void SendDocuments(Account account, List<IFormFile> files, bool isForeign = false)
        {

        }

        public void SendSms(Account account, string text)
        {

        }
    }
}
