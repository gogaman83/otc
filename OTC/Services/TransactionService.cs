﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;
using OTC.Data;

namespace OTC.Services
{
    public class TransactionService : ITransactionService
    {
        private IRepository _repository;
        private IAccountTransactionProcessor _accountTransactionProcessor;
        private IBankTransactionProcessor _bankTransactionProcessor;

        public TransactionService(IRepository repository, IAccountTransactionProcessor accountTransactionProcessor, IBankTransactionProcessor bankTransactionProcessor)
        {
            _repository = repository;
            _accountTransactionProcessor = accountTransactionProcessor;
            _bankTransactionProcessor = bankTransactionProcessor;
        }

        public void AddTransaction(Transaction transaction)
        {
            transaction.Date = DateTime.Now;

            transaction.Sender = _repository.GetAccount(transaction.SenderId);
            transaction.Recipient = _repository.GetAccount(transaction.RecipientId);

            if (transaction.Recipient == null)
                throw new TransactionException("Не найден счет-получатель");
            else if (transaction.Sender == null)
                throw new TransactionException("Не найден счет-отправитель");
            else if (transaction.Sender.Amount < transaction.Amount)
                throw new TransactionException("Сумма транзакции больше суммы на счете", "amount");
            else if (transaction.Sender.Bank.RequireConfirmation && !transaction.Confirmed)
                throw new TransactionException("Требуется подтверждение");
            else
            {
                transaction.BankCommission = this.GetBankComission(transaction);
                transaction.TransferCommission = this.GetTransfersComission(transaction);
                transaction.Sender.Amount -= (transaction.Amount + transaction.BankCommission + transaction.TransferCommission);
                _repository.AddTransaction(transaction);
                _repository.Save();

                try
                {
                    transaction.Sender.Bank.OnTransactionCreated(_bankTransactionProcessor, transaction);
                }
                catch (Exception exception)
                {

                }

                try
                {
                    transaction.Sender.AccountType.OnTransactionCreated(_accountTransactionProcessor, transaction);
                }
                catch(Exception exception)
                {

                }
            }
        }

        protected double GetBankComission(Transaction transaction)
        {
            return transaction.Amount*((transaction.Sender.BankId == transaction.Recipient.BankId ? transaction.Sender.Bank.ComssionSelf : transaction.Sender.Bank.ComssionAnother) / 100);
        }

        protected double GetTransfersComission(Transaction transaction)
        {
            //Сопоставление комиссий для разных типов счетов
            Dictionary<Tuple<Type, Type>, double> transferCommisions = new Dictionary<Tuple<Type, Type>, double> {
                {  Tuple.Create(typeof(PhysicalAccountType), typeof(PhysicalAccountType)), 0 },
                {  Tuple.Create(typeof(PhysicalAccountType), typeof(LegalAccountType)), 0 },
                {  Tuple.Create(typeof(PhysicalAccountType), typeof(NonResidentAccountType)), 0 },
                {  Tuple.Create(typeof(LegalAccountType), typeof(PhysicalAccountType)), 2 },
                {  Tuple.Create(typeof(LegalAccountType), typeof(LegalAccountType)), 3 },
                {  Tuple.Create(typeof(LegalAccountType), typeof(NonResidentAccountType)), 4 },
                {  Tuple.Create(typeof(NonResidentAccountType), typeof(PhysicalAccountType)), 4 },
                {  Tuple.Create(typeof(NonResidentAccountType), typeof(LegalAccountType)), 6 },
                {  Tuple.Create(typeof(NonResidentAccountType), typeof(NonResidentAccountType)), 6 },
            };

            var transactionTypes = Tuple.Create(transaction.Sender.AccountType.GetType(), transaction.Recipient.AccountType.GetType());

            if (transferCommisions.ContainsKey(transactionTypes))
            {
                return transaction.Amount * (transferCommisions[transactionTypes] / 100);
            }
            else
                throw new TransactionException("Ошибка вычисления комиссии");
        }
    }


    class TransactionException : Exception
    {
        public string field { get; set; }
        public TransactionException(string message, string field = null) : base(message)
        {
            this.field = field == null ? "error" : field;
        }
    }
}
