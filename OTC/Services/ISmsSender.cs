﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Services
{
    public interface ISmsSender
    {
        void Send(string phoneNumber, string text);
    }
}
