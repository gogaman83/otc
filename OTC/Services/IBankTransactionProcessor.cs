﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;

namespace OTC.Services
{
    public interface IBankTransactionProcessor
    {
        void SendToTax(Transaction transaction);

        void SendToPartnerBank(Transaction transaction, Bank partnerBank);
    }
}
