﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;

namespace OTC.Services
{
    public interface ITransactionService
    {
        public void AddTransaction(Transaction transaction);
    }
}
