﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;
using Microsoft.AspNetCore.Http;

namespace OTC.Services
{
    public interface IAccountTransactionProcessor
    {
        public void SendDocuments(Account account, List<IFormFile> files, bool isForeign = false);

        public void SendSms(Account account, string text);
    }
}
