﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Models;

namespace OTC.Services
{
    public class BankTransactionProcessor : IBankTransactionProcessor
    {
        private IEmailSender _emailSender;

        public BankTransactionProcessor(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public void SendToTax(Transaction transaction)
        {

        }

        public void SendToPartnerBank(Transaction transaction, Bank partnerBank)
        {

        }
    }
}
