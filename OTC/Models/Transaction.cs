﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Components;

namespace OTC.Models
{
    public class Transaction : IValidatableObject
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        public int RecipientId { get; set; }
        public double Amount { get; set; }
        public double TransferCommission { get; set; }
        public double BankCommission { get; set; }
        public DateTime Date { get; set; }
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        [ForeignKey("SenderId")]
        public Account Sender { get; set; }
        [ForeignKey("RecipientId")]
        public Account Recipient { get; set; }

        [NotMapped]
        public bool Confirmed { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.Amount == 0)
            {
                yield return new ValidationResult(
                    $"Заполните сумму",
                    new[] { nameof(this.Amount) });
            }
        }
    }
}
