﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Services;

namespace OTC.Models
{
    class LegalAccountType : AccountType
    {
        public override AccountTypeId Id => AccountTypeId.legal;
        public override string Name => "Юридическое лицо";

        public override void OnTransactionCreated(IAccountTransactionProcessor processor, Transaction transaction)
        {
            processor.SendDocuments(transaction.Sender, new List<Microsoft.AspNetCore.Http.IFormFile>());
        }
    }
}
