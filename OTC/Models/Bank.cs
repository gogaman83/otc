﻿using OTC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Models
{
    public enum BankId
    {
        sbrf = 0,
        vtb = 1,
        alpha = 2
    }
    public abstract class Bank
    {
        abstract public BankId Id { get; }
        abstract public string Name { get; }
        abstract public double ComssionSelf { get; }
        abstract public double ComssionAnother { get; }
        abstract public bool RequireConfirmation { get; }

        public virtual void OnTransactionCreated(IBankTransactionProcessor processor, Transaction transaction)
        {
            
        }
    }
}
