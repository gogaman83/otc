﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Services;

namespace OTC.Models
{
    class NonResidentAccountType : AccountType
    {
        public override AccountTypeId Id => AccountTypeId.nonresident;
        public override string Name => "Нерезидент";

        public override void OnTransactionCreated(IAccountTransactionProcessor processor, Transaction transaction)
        {
            processor.SendDocuments(transaction.Sender, new List<Microsoft.AspNetCore.Http.IFormFile>(), true);
        }
    }
}
