﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Models
{
    public class Account
    {
        public int Id { get; set; }
        [Required]
        public string UID { get; set; }
        public AccountTypeId AccountTypeId { get; set; }
        public BankId BankId { get; set; }
        public double Amount { get; set; }

        public List<Transaction> OutTransactions { get; set; }
        public List<Transaction> InTransactions { get; set; }

        public AccountType AccountType
        {
            get
            {
                return Helpers.GetEnumerableOfType<AccountType>().FirstOrDefault(x => x.Id == this.AccountTypeId);
            }
        }

        public Bank Bank
        {
            get
            {
                return Helpers.GetEnumerableOfType<Bank>().FirstOrDefault(x => x.Id == this.BankId);
            }
        }
    }
}
