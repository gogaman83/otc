﻿using OTC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Models
{
    class SberBank : Bank
    {
        public override BankId Id => BankId.sbrf;
        public override string Name => "Сбербанк";
        public override double ComssionSelf => 0;
        public override double ComssionAnother => 1;
        public override bool RequireConfirmation => false;

        public override void OnTransactionCreated(IBankTransactionProcessor processor, Transaction transaction)
        {
            processor.SendToTax(transaction);
        }
    }
}
