﻿using OTC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Models
{
    class PhysicalAccountType : AccountType
    {
        public override AccountTypeId Id => AccountTypeId.physical;
        public override string Name => "Физическое лицо";

        public override void OnTransactionCreated(IAccountTransactionProcessor processor, Transaction transaction)
        {
            processor.SendSms(transaction.Sender, "Вы совершили платеж");
        }
    }
}
