﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Services;

namespace OTC.Models
{
    class VTBank : Bank
    {
        public override BankId Id => BankId.vtb;
        public override string Name => "ВТБ";
        public override double ComssionSelf => 0;
        public override double ComssionAnother => 2;
        public override bool RequireConfirmation => false;

        public override void OnTransactionCreated(IBankTransactionProcessor processor, Transaction transaction)
        {
            processor.SendToPartnerBank(transaction, transaction.Recipient.Bank);
        }
    }
}
