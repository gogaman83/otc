﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTC.Models
{
    class AlphaBank : Bank
    {
        public override BankId Id => BankId.alpha;
        public override string Name => "Альфабанк";
        public override double ComssionSelf => 1;
        public override double ComssionAnother => 2.5;
        public override bool RequireConfirmation => true;
    }
}
