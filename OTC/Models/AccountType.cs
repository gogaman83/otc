﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OTC.Services;

namespace OTC.Models
{
    public enum AccountTypeId
    {
        physical = 0,
        legal = 1,
        nonresident = 2
    }
    public abstract class AccountType
    {
        abstract public AccountTypeId Id { get; }
        abstract public string Name { get; }

        public virtual void OnTransactionCreated(IAccountTransactionProcessor processor, Transaction transaction)
        {

        }
    }
}
