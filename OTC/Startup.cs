using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using OTC.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OTC.Models;
using OTC.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using System.Text.Json;

namespace OTC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<ApplicationUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddScoped<IRepository, EFRepository>();

            //���������� �������
            services.AddSingleton<ISmsSender, SmsSender>();
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddSingleton<IAccountTransactionProcessor, AccountTransactionProcessor>();
            services.AddSingleton<IBankTransactionProcessor, BankTransactionProcessor>();
            services.AddScoped<ITransactionService, TransactionService>();

            services.AddControllersWithViews();

            services.AddRazorPages();

            services.AddHttpContextAccessor();

            services.AddMvc().AddNewtonsoftJson(mvcNewtonsoftJsonOptions => mvcNewtonsoftJsonOptions.UseCamelCasing(processDictionaryKeys: true));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<ApplicationUser> userManager)
        {
            //������� ���������
            var operatorLogin = "operator@mail.ru";
            if (userManager.FindByNameAsync(operatorLogin).Result == null)
            {
                var result = userManager.CreateAsync(new ApplicationUser
                {
                    Email = operatorLogin,
                    UserName = operatorLogin
                }, "Operator_1234").Result;
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
